.\" Man page generated from reStructuredText.
.
.TH "BEET" "1" "Nov 27, 2021" "1.6" "beets"
.SH NAME
beet \- music tagger and library organizer
.
.nr rst2man-indent-level 0
.
.de1 rstReportMargin
\\$1 \\n[an-margin]
level \\n[rst2man-indent-level]
level margin: \\n[rst2man-indent\\n[rst2man-indent-level]]
-
\\n[rst2man-indent0]
\\n[rst2man-indent1]
\\n[rst2man-indent2]
..
.de1 INDENT
.\" .rstReportMargin pre:
. RS \\$1
. nr rst2man-indent\\n[rst2man-indent-level] \\n[an-margin]
. nr rst2man-indent-level +1
.\" .rstReportMargin post:
..
.de UNINDENT
. RE
.\" indent \\n[an-margin]
.\" old: \\n[rst2man-indent\\n[rst2man-indent-level]]
.nr rst2man-indent-level -1
.\" new: \\n[rst2man-indent\\n[rst2man-indent-level]]
.in \\n[rst2man-indent\\n[rst2man-indent-level]]u
..
.SH SYNOPSIS
.nf
\fBbeet\fP [\fIargs\fP…] \fIcommand\fP [\fIargs\fP…]
\fBbeet help\fP \fIcommand\fP
.fi
.sp
.SH COMMANDS
.SS import
.INDENT 0.0
.INDENT 3.5
.sp
.nf
.ft C
beet import [\-CWAPRqst] [\-l LOGPATH] PATH...
beet import [options] \-L QUERY
.ft P
.fi
.UNINDENT
.UNINDENT
.sp
Add music to your library, attempting to get correct tags for it from
MusicBrainz.
.sp
Point the command at some music: directories, single files, or
compressed archives. The music will be copied to a configurable
directory structure and added to a library database. The command is
interactive and will try to get you to verify MusicBrainz tags that it
thinks are suspect. See the autotagging guide
for detail on how to use the interactive tag\-correction flow.
.sp
Directories passed to the import command can contain either a single
album or many, in which case the leaf directories will be considered
albums (the latter case is true of typical Artist/Album organizations
and many people’s “downloads” folders). The path can also be a single
song or an archive. Beets supports \fIzip\fP and \fItar\fP archives out of the
box. To extract \fIrar\fP files, install the \fI\%rarfile\fP package and the
\fIunrar\fP command. To extract \fI7z\fP files, install the \fI\%py7zr\fP package.
.sp
Optional command flags:
.INDENT 0.0
.IP \(bu 2
By default, the command copies files to your library directory and
updates the ID3 tags on your music. In order to move the files, instead of
copying, use the \fB\-m\fP (move) option. If you’d like to leave your music
files untouched, try the \fB\-C\fP (don’t copy) and \fB\-W\fP (don’t write tags)
options. You can also disable this behavior by default in the
configuration file (below).
.IP \(bu 2
Also, you can disable the autotagging behavior entirely using \fB\-A\fP
(don’t autotag)—then your music will be imported with its existing
metadata.
.IP \(bu 2
During a long tagging import, it can be useful to keep track of albums
that weren’t tagged successfully—either because they’re not in the
MusicBrainz database or because something’s wrong with the files. Use the
\fB\-l\fP option to specify a filename to log every time you skip an album
or import it “as\-is” or an album gets skipped as a duplicate.
.IP \(bu 2
Relatedly, the \fB\-q\fP (quiet) option can help with large imports by
autotagging without ever bothering to ask for user input. Whenever the
normal autotagger mode would ask for confirmation, the quiet mode
pessimistically skips the album. The quiet mode also disables the tagger’s
ability to resume interrupted imports.
.IP \(bu 2
Speaking of resuming interrupted imports, the tagger will prompt you if it
seems like the last import of the directory was interrupted (by you or by
a crash). If you want to skip this prompt, you can say “yes” automatically
by providing \fB\-p\fP or “no” using \fB\-P\fP\&. The resuming feature can be
disabled by default using a configuration option (see below).
.IP \(bu 2
If you want to import only the \fInew\fP stuff from a directory, use the
\fB\-i\fP
option to run an \fIincremental\fP import. With this flag, beets will keep
track of every directory it ever imports and avoid importing them again.
This is useful if you have an “incoming” directory that you periodically
add things to.
To get this to work correctly, you’ll need to use an incremental import \fIevery
time\fP you run an import on the directory in question—including the first
time, when no subdirectories will be skipped. So consider enabling the
\fBincremental\fP configuration option.
.IP \(bu 2
When beets applies metadata to your music, it will retain the value of any
existing tags that weren’t overwritten, and import them into the database. You
may prefer to only use existing metadata for finding matches, and to erase it
completely when new metadata is applied. You can enforce this behavior with
the \fB\-\-from\-scratch\fP option, or the \fBfrom_scratch\fP configuration option.
.IP \(bu 2
By default, beets will proceed without asking if it finds a very close
metadata match. To disable this and have the importer ask you every time,
use the \fB\-t\fP (for \fItimid\fP) option.
.IP \(bu 2
The importer typically works in a whole\-album\-at\-a\-time mode. If you
instead want to import individual, non\-album tracks, use the \fIsingleton\fP
mode by supplying the \fB\-s\fP option.
.IP \(bu 2
If you have an album that’s split across several directories under a common
top directory, use the \fB\-\-flat\fP option. This takes all the music files
under the directory (recursively) and treats them as a single large album
instead of as one album per directory. This can help with your more stubborn
multi\-disc albums.
.IP \(bu 2
Similarly, if you have one directory that contains multiple albums, use the
\fB\-\-group\-albums\fP option to split the files based on their metadata before
matching them as separate albums.
.IP \(bu 2
If you want to preview which files would be imported, use the \fB\-\-pretend\fP
option. If set, beets will just print a list of files that it would
otherwise import.
.IP \(bu 2
If you already have a metadata backend ID that matches the items to be
imported, you can instruct beets to restrict the search to that ID instead of
searching for other candidates by using the \fB\-\-search\-id SEARCH_ID\fP option.
Multiple IDs can be specified by simply repeating the option several times.
.IP \(bu 2
You can supply \fB\-\-set field=value\fP to assign \fIfield\fP to \fIvalue\fP on import.
These assignments will merge with (and possibly override) the
set_fields configuration dictionary. You can use the option multiple
times on the command line, like so:
.INDENT 2.0
.INDENT 3.5
.sp
.nf
.ft C
beet import \-\-set genre="Alternative Rock" \-\-set mood="emotional"
.ft P
.fi
.UNINDENT
.UNINDENT
.UNINDENT
.SS list
.INDENT 0.0
.INDENT 3.5
.sp
.nf
.ft C
beet list [\-apf] QUERY
.ft P
.fi
.UNINDENT
.UNINDENT
.sp
Queries the database for music.
.sp
Want to search for “Gronlandic Edit” by of Montreal? Try \fBbeet list
gronlandic\fP\&.  Maybe you want to see everything released in 2009 with
“vegetables” in the title? Try \fBbeet list year:2009 title:vegetables\fP\&. You
can also specify the sort order. (Read more in query\&.)
.sp
You can use the \fB\-a\fP switch to search for albums instead of individual items.
In this case, the queries you use are restricted to album\-level fields: for
example, you can search for \fByear:1969\fP but query parts for item\-level fields
like \fBtitle:foo\fP will be ignored. Remember that \fBartist\fP is an item\-level
field; \fBalbumartist\fP is the corresponding album field.
.sp
The \fB\-p\fP option makes beets print out filenames of matched items, which might
be useful for piping into other Unix commands (such as \fI\%xargs\fP). Similarly, the
\fB\-f\fP option lets you specify a specific format with which to print every album
or track. This uses the same template syntax as beets’ path formats\&. For example, the command \fBbeet ls \-af \(aq$album: $albumtotal\(aq
beatles\fP prints out the number of tracks on each Beatles album. In Unix shells,
remember to enclose the template argument in single quotes to avoid environment
variable expansion.
.SS remove
.INDENT 0.0
.INDENT 3.5
.sp
.nf
.ft C
beet remove [\-adf] QUERY
.ft P
.fi
.UNINDENT
.UNINDENT
.sp
Remove music from your library.
.sp
This command uses the same query syntax as the \fBlist\fP command.
By default, it just removes entries from the library database; it doesn’t
touch the files on disk. To actually delete the files, use the \fB\-d\fP flag.
When the \fB\-a\fP flag is given, the command operates on albums instead of
individual tracks.
.sp
When you run the \fBremove\fP command, it prints a list of all
affected items in the library and asks for your permission before removing
them. You can then choose to abort (type \fIn\fP), confirm (\fIy\fP), or interactively
choose some of the items (\fIs\fP). In the latter case, the command will prompt you
for every matching item or album and invite you to type \fIy\fP to remove the
item/album, \fIn\fP to keep it or \fIq\fP to exit and only remove the items/albums
selected up to this point.
This option lets you choose precisely which tracks/albums to remove without
spending too much time to carefully craft a query.
If you do not want to be prompted at all, use the \fB\-f\fP option.
.SS modify
.INDENT 0.0
.INDENT 3.5
.sp
.nf
.ft C
beet modify [\-MWay] [\-f FORMAT] QUERY [FIELD=VALUE...] [FIELD!...]
.ft P
.fi
.UNINDENT
.UNINDENT
.sp
Change the metadata for items or albums in the database.
.sp
Supply a query matching the things you want to change and a
series of \fBfield=value\fP pairs. For example, \fBbeet modify genius of love
artist="Tom Tom Club"\fP will change the artist for the track “Genius of Love.”
To remove fields (which is only possible for flexible attributes), follow a
field name with an exclamation point: \fBfield!\fP\&.
.sp
The \fB\-a\fP switch operates on albums instead of individual tracks. Without
this flag, the command will only change \fItrack\-level\fP data, even if all the
tracks belong to the same album. If you want to change an \fIalbum\-level\fP field,
such as \fByear\fP or \fBalbumartist\fP, you’ll want to use the \fB\-a\fP flag to
avoid a confusing situation where the data for individual tracks conflicts
with the data for the whole album.
.sp
Items will automatically be moved around when necessary if they’re in your
library directory, but you can disable that with  \fB\-M\fP\&. Tags will be written
to the files according to the settings you have for imports, but these can be
overridden with \fB\-w\fP (write tags, the default) and \fB\-W\fP (don’t write
tags).
.sp
When you run the \fBmodify\fP command, it prints a list of all
affected items in the library and asks for your permission before making any
changes. You can then choose to abort the change (type \fIn\fP), confirm
(\fIy\fP), or interactively choose some of the items (\fIs\fP). In the latter case,
the command will prompt you for every matching item or album and invite you to
type \fIy\fP to apply the changes, \fIn\fP to discard them or \fIq\fP to exit and apply
the selected changes. This option lets you choose precisely which data to
change without spending too much time to carefully craft a query. To skip the
prompts entirely, use the \fB\-y\fP option.
.SS move
.INDENT 0.0
.INDENT 3.5
.sp
.nf
.ft C
beet move [\-capt] [\-d DIR] QUERY
.ft P
.fi
.UNINDENT
.UNINDENT
.sp
Move or copy items in your library.
.sp
This command, by default, acts as a library consolidator: items matching the
query are renamed into your library directory structure. By specifying a
destination directory with \fB\-d\fP manually, you can move items matching a query
anywhere in your filesystem. The \fB\-c\fP option copies files instead of moving
them. As with other commands, the \fB\-a\fP option matches albums instead of items.
The \fB\-e\fP flag (for “export”) copies files without changing the database.
.sp
To perform a “dry run”, just use the \fB\-p\fP (for “pretend”) flag. This will
show you a list of files that would be moved but won’t actually change anything
on disk. The \fB\-t\fP option sets the timid mode which will ask again
before really moving or copying the files.
.SS update
.INDENT 0.0
.INDENT 3.5
.sp
.nf
.ft C
beet update [\-F] FIELD [\-aM] QUERY
.ft P
.fi
.UNINDENT
.UNINDENT
.sp
Update the library (and, by default, move files) to reflect out\-of\-band metadata
changes and file deletions.
.sp
This will scan all the matched files and read their tags, populating the
database with the new values. By default, files will be renamed according to
their new metadata; disable this with \fB\-M\fP\&. Beets will skip files if their
modification times have not changed, so any out\-of\-band metadata changes must
also update these for \fBbeet update\fP to recognise that the files have been
edited.
.sp
To perform a “dry run” of an update, just use the \fB\-p\fP (for “pretend”) flag.
This will show you all the proposed changes but won’t actually change anything
on disk.
.sp
By default, all the changed metadata will be populated back to the database.
If you only want certain fields to be written, specify them with the \fB\(ga\-F\(ga\fP
flags (which can be used multiple times). For the list of supported fields,
please see \fB\(gabeet fields\(ga\fP\&.
.sp
When an updated track is part of an album, the album\-level fields of \fIall\fP
tracks from the album are also updated. (Specifically, the command copies
album\-level data from the first track on the album and applies it to the
rest of the tracks.) This means that, if album\-level fields aren’t identical
within an album, some changes shown by the \fBupdate\fP command may be
overridden by data from other tracks on the same album. This means that
running the \fBupdate\fP command multiple times may show the same changes being
applied.
.SS write
.INDENT 0.0
.INDENT 3.5
.sp
.nf
.ft C
beet write [\-pf] [QUERY]
.ft P
.fi
.UNINDENT
.UNINDENT
.sp
Write metadata from the database into files’ tags.
.sp
When you make changes to the metadata stored in beets’ library database
(during import or with the \fI\%modify\fP command, for example), you often
have the option of storing changes only in the database, leaving your files
untouched. The \fBwrite\fP command lets you later change your mind and write the
contents of the database into the files. By default, this writes the changes only if there is a difference between the database and the tags in the file.
.sp
You can think of this command as the opposite of \fI\%update\fP\&.
.sp
The \fB\-p\fP option previews metadata changes without actually applying them.
.sp
The \fB\-f\fP option forces a write to the file, even if the file tags match the database. This is useful for making sure that enabled plugins that run on write (e.g., the Scrub and Zero plugins) are run on the file.
.SS stats
.INDENT 0.0
.INDENT 3.5
.sp
.nf
.ft C
beet stats [\-e] [QUERY]
.ft P
.fi
.UNINDENT
.UNINDENT
.sp
Show some statistics on your entire library (if you don’t provide a
query) or the matched items (if you do).
.sp
By default, the command calculates file sizes using their bitrate and
duration. The \fB\-e\fP (\fB\-\-exact\fP) option reads the exact sizes of each file
(but is slower). The exact mode also outputs the exact duration in seconds.
.SS fields
.INDENT 0.0
.INDENT 3.5
.sp
.nf
.ft C
beet fields
.ft P
.fi
.UNINDENT
.UNINDENT
.sp
Show the item and album metadata fields available for use in query and
pathformat\&. The listing includes any template fields provided by
plugins and any flexible attributes you’ve manually assigned to your items and
albums.
.SS config
.INDENT 0.0
.INDENT 3.5
.sp
.nf
.ft C
beet config [\-pdc]
beet config \-e
.ft P
.fi
.UNINDENT
.UNINDENT
.sp
Show or edit the user configuration. This command does one of three things:
.INDENT 0.0
.IP \(bu 2
With no options, print a YAML representation of the current user
configuration. With the \fB\-\-default\fP option, beets’ default options are
also included in the dump.
.IP \(bu 2
The \fB\-\-path\fP option instead shows the path to your configuration file.
This can be combined with the \fB\-\-default\fP flag to show where beets keeps
its internal defaults.
.IP \(bu 2
By default, sensitive information like passwords is removed when dumping the
configuration. The \fB\-\-clear\fP option includes this sensitive data.
.IP \(bu 2
With the \fB\-\-edit\fP option, beets attempts to open your config file for
editing. It first tries the \fB$EDITOR\fP environment variable and then a
fallback option depending on your platform: \fBopen\fP on OS X, \fBxdg\-open\fP
on Unix, and direct invocation on Windows.
.UNINDENT
.SH GLOBAL FLAGS
.sp
Beets has a few “global” flags that affect all commands. These must appear
between the executable name (\fBbeet\fP) and the command—for example, \fBbeet \-v
import ...\fP\&.
.INDENT 0.0
.IP \(bu 2
\fB\-l LIBPATH\fP: specify the library database file to use.
.IP \(bu 2
\fB\-d DIRECTORY\fP: specify the library root directory.
.IP \(bu 2
\fB\-v\fP: verbose mode; prints out a deluge of debugging information. Please use
this flag when reporting bugs. You can use it twice, as in \fB\-vv\fP, to make
beets even more verbose.
.IP \(bu 2
\fB\-c FILE\fP: read a specified YAML configuration file\&. This
configuration works as an overlay: rather than replacing your normal
configuration options entirely, the two are merged. Any individual options set
in this config file will override the corresponding settings in your base
configuration.
.IP \(bu 2
\fB\-p plugins\fP: specify a comma\-separated list of plugins to enable. If
specified, the plugin list in your configuration is ignored. The long form
of this argument also allows specifying no plugins, effectively disabling
all plugins: \fB\-\-plugins=\fP\&.
.UNINDENT
.sp
Beets also uses the \fBBEETSDIR\fP environment variable to look for
configuration and data.
.SH SHELL COMPLETION
.sp
Beets includes support for shell command completion. The command \fBbeet
completion\fP prints out a \fI\%bash\fP 3.2 script; to enable completion put a line
like this into your \fB\&.bashrc\fP or similar file:
.INDENT 0.0
.INDENT 3.5
.sp
.nf
.ft C
eval "$(beet completion)"
.ft P
.fi
.UNINDENT
.UNINDENT
.sp
Or, to avoid slowing down your shell startup time, you can pipe the \fBbeet
completion\fP output to a file and source that instead.
.sp
You will also need to source the \fI\%bash\-completion\fP script, which is probably
available via your package manager. On OS X, you can install it via Homebrew
with \fBbrew install bash\-completion\fP; Homebrew will give you instructions for
sourcing the script.
.sp
The completion script suggests names of subcommands and (after typing
\fB\-\fP) options of the given command. If you are using a command that
accepts a query, the script will also complete field names.
.INDENT 0.0
.INDENT 3.5
.sp
.nf
.ft C
beet list ar[TAB]
# artist:  artist_credit:  artist_sort:  artpath:
beet list artp[TAB]
beet list artpath\e:
.ft P
.fi
.UNINDENT
.UNINDENT
.sp
(Don’t worry about the slash in front of the colon: this is a escape
sequence for the shell and won’t be seen by beets.)
.sp
Completion of plugin commands only works for those plugins
that were enabled when running \fBbeet completion\fP\&. If you add a plugin
later on you will want to re\-generate the script.
.SS zsh
.sp
If you use zsh, take a look at the included \fI\%completion script\fP\&. The script
should be placed in a directory that is part of your \fBfpath\fP, and \fInot\fP
sourced in your \fB\&.zshrc\fP\&. Running \fBecho $fpath\fP will give you a list of
valid directories.
.sp
Another approach is to use zsh’s bash completion compatibility. This snippet
defines some bash\-specific functions to make this work without errors:
.INDENT 0.0
.INDENT 3.5
.sp
.nf
.ft C
autoload bashcompinit
bashcompinit
_get_comp_words_by_ref() { :; }
compopt() { :; }
_filedir() { :; }
eval "$(beet completion)"
.ft P
.fi
.UNINDENT
.UNINDENT
.SH SEE ALSO
.sp
\fBhttps://beets.readthedocs.org/\fP
.sp
\fBbeetsconfig(5)\fP
.SH AUTHOR
Adrian Sampson
.SH COPYRIGHT
2016, Adrian Sampson
.\" Generated by docutils manpage writer.
.
